package mz.co.vm.services;


import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.json.JsonObject;

import mz.co.vm.models.RandomNumber;

/**
 * 
 * @author Elton Tomas Laice
 *
 */
@ApplicationScoped
public class RandomNumberService {
	public JsonObject getJsonRandomNumber(RandomNumber randomNumber) {
    	JsonObject model = Json.createObjectBuilder()
 			   .add("requestID", randomNumber.getRequestID())
 			   .add("number", randomNumber.getNumber())
 			   .add("process_time", randomNumber.getProcess_time())
 			   .build();
    	return model;
	}
}
