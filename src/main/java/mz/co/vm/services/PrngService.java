package mz.co.vm.services;

import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.ehcache.Cache;

import mz.co.vm.models.RandomNumber;

/**
 * Services for PRNG resources
 * 
 * @author Elton Tomas Laice
 * @see mz.co.vm.controllers.PrngController
 */
@ApplicationScoped
public class PrngService {
	
	@Inject
	private CacheService cacheService;
	/**
	 * Success information if random number was created
	 * @return json
	 */
	public JsonObject success_JsonRandomNumber(String requestId) {
    	JsonObject json = Json.createObjectBuilder()
 			   .add("status", "success")
 			   .add("requestId", requestId)
 			   .add("message", "Request created.")
 			   .build();
    	return json;
	}
	
	/**
	 * The IDs generated must be 128-bit GUIDs
	 * @return requestID
	 */
	public String create_id_request() {
		UUID uuid = UUID.randomUUID();
		String requestID = uuid.toString();
		return requestID;
	}
	
	/**
	 * Store Number class in H2 database memory
	 * @param RequestId
	 */
	public void createNumber(String requestId){
		RandomNumber randomNumber = new RandomNumber(requestId);
		if(cacheService.getTimeRequestCacheFromCacheManager().containsKey(requestId)) 
			randomNumber.setProcess_time(cacheService.getTimeRequestCacheFromCacheManager().get(requestId));
	    cacheService.getRandomNumberCacheFromCacheManager().put(requestId, randomNumber);
	}
	
	/**
	 * Returns a List of Generated Random Numbers.
	 * This includes the time that was taken to process
	 * each random number.
	 * 
	 * @return JsonObject
	 */
	public JsonObject getHistory() {
		Iterator<Cache.Entry<String, RandomNumber>> iterator = cacheService.getRandomNumberCacheFromCacheManager().iterator();
		
		JsonArrayBuilder json_numbers = Json.createArrayBuilder();
		while (iterator.hasNext()) {
	        RandomNumber randomNumber = (RandomNumber) iterator.next().getValue();
	    	JsonObject json_object = Json.createObjectBuilder()
	  			   .add("requestID", randomNumber.getRequestID())
	  			   .add("number", randomNumber.getNumber())
	  			   .add("process_time", randomNumber.getProcess_time())
	  			   .add("status", randomNumber.getStatus())
	  			   .build();
	    	json_numbers.add(json_object);
	    }
		
		JsonObjectBuilder json_main = Json.createObjectBuilder()
				.add("status", "success")
				.add("random_numbers", json_numbers);
     	return json_main.build();
	}
	
	/**
	 * Cancels a Random Number Request that is pending
	 * @param requestId
	 * @return JsonObject
	 */
	public JsonObject cancel(String requestId) {
		Cache<String, RandomNumber> cache = cacheService.getRandomNumberCacheFromCacheManager();
		
		if(cache.containsKey(requestId)) {
			if(cache.get(requestId).getStatus().equals("PENDING")) {
				cache.remove(requestId);
		    	JsonObject json = Json.createObjectBuilder()
		   			   .add("status", "success")
		   			   .add("requestId", requestId)
		   			   .add("message", "Random Number canceled")
		   			   .build();
		      	return json;		
			}else {
		    	JsonObject json = Json.createObjectBuilder()
			   			   .add("status", "warring")
			   			   .add("requestId", requestId)
			   			   .add("message", "Process has already been completed")
			   			   .build();
		    	return json;
			}
		}
    	JsonObject json = Json.createObjectBuilder()
	   			   .add("status", "not_found")
	   			   .add("requestId", requestId)
	   			   .add("message", "Number doesn't Exist")
	   			   .build();
    	return json;
	}
	
	/**
	 * Get system usage statistics:
	 * @return JsonObject
	 */
	public JsonObject stats() {
		Iterator<Cache.Entry<String, RandomNumber>> iterator = cacheService.getRandomNumberCacheFromCacheManager().iterator();
		JsonObject json = Json.createObjectBuilder()
	   			   .add("maximum_waiting_time", getMaximumWaitingTime(iterator))
	   			   .add("minimum_waiting_time", getMinimummWaitingTime(iterator))
	   			   .add("total_pending_requests", getTotalPendingRequests(iterator))
	   			   .build();
    	return json;
	}
	
	/**
	 * Maximum waiting time- the maximum time that has been spent to process a Random Request
	 * 
	 * @param iterator
	 * @return double
	 */
	public double getMaximumWaitingTime(Iterator<Cache.Entry<String, RandomNumber>> iterator) {
		double maximum = 0, actualTime = 0;
		while (iterator.hasNext()) {
			actualTime = iterator.next().getValue().getProcess_time();
			if(actualTime > maximum)
				maximum = actualTime;
		}
		return maximum;
	}
	
	/**
	 * Minimum waiting time – the minimum time that has been spent to process a Random number request
	 * @param iterator
	 * @return double
	 */
	public double getMinimummWaitingTime(Iterator<Cache.Entry<String, RandomNumber>> iterator) {
		double minimum = 0, actualTime = 0;
		while (iterator.hasNext()) {
			actualTime = iterator.next().getValue().getProcess_time();
			if(actualTime < minimum)
				minimum = actualTime;
		}
		return minimum;
	}
	
	/**
	 * Total pending Requests – the total Random Number requests that are currently pending
	 * 
	 * @param iterator
	 * @return
	 */
	public double getTotalPendingRequests(Iterator<Cache.Entry<String, RandomNumber>> iterator) {
		int totalPendingRequests = 0;
		while (iterator.hasNext()) {
			if(iterator.next().getValue().getStatus().equals("PENDING"))
				totalPendingRequests++;
		}
		return totalPendingRequests;
	}
}
