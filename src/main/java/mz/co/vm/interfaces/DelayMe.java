package mz.co.vm.interfaces;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * The application must have a @DelayMe annotation, which should cause any method
 * that is annotated with it to be delayed by 20 seconds. The annotation should receive
 * an optional parameter to define the delay time: @DelayMe(time=2000).
 * @author Elton Tomas Laice
 * 
 *
 */
@NameBinding
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface DelayMe {}
