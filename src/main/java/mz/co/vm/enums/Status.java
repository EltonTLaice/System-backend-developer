package mz.co.vm.enums;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public enum Status {
	PENDING,
	CREATED
}
