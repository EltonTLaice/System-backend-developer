package mz.co.vm.filters;

import java.io.IOException;
import java.time.Instant;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.ext.Provider;

import mz.co.vm.interfaces.Duration;

/**
 * The DurationFilter Class represents the filter that treats requests 
 * and replenishes every application, allowing to calculate the duration 
 * of the requests
 * 
 * 
 * @author Elton Tomas Laice
 * @see mz.co.vm.interfaces.Duration
 *
 */

@Provider
@Duration
public class DurationFilter implements ContainerRequestFilter, ContainerResponseFilter{
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		long request_time = Instant.now().getNano();
		requestContext.getHeaders().add("X-Request-Duration-Start", request_time + "");
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		responseContext.getHeaders().add("X-Request-Duration", get_request_time(requestContext, responseContext));
		
		/**
		 * Store Total Time of Request
		 */
		String requestId = responseContext.getHeaderString("Request-Id");
	}
	
	/**
	 * calculate and make difference between Time to get request and give response 
	 * @param requestContext
	 * @param responseContext
	 * @return
	 */
	public double get_request_time(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
		long start_time = Long.parseLong(requestContext.getHeaderString("X-Request-Duration-Start"));
		long duration = Instant.now().getNano() - start_time;
		return convert_nano_seconds(duration);		
	}
	
	/**
	 *  Convert Nano to seconds
	 * @param nano
	 * @return 
	 */
	public double convert_nano_seconds(long nano) {
		double seconds = (double)nano / 1_000_000_000.0;
		return seconds;
	}
}
