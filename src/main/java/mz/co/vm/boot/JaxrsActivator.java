package mz.co.vm.boot;


import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import mz.co.vm.services.CacheService;

/**
 * Boot Application class
 * @author Eton Tomas Laice
 *
 */
@ApplicationPath("/")
public class JaxrsActivator extends Application {
	/**
	 * CacheService call initialize Cache Service to store data in memory
	 */
	@Inject
	private CacheService cacheService;
}
