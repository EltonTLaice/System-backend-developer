package mz.co.vm.controllers;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import mz.co.vm.interfaces.Duration;
import mz.co.vm.interfaces.StoreTime;
import mz.co.vm.services.CacheService;
import mz.co.vm.services.PrngService;
import mz.co.vm.services.RandomNumberService;

import static javax.ws.rs.core.Response.ok;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@Duration
public class PrngController {
	@Inject
	private CacheService cacheService;
	
	@Inject
	private RandomNumberService numberService;
	
	@Inject
	private PrngService prngService;
	
//	@Inject
//	@ConfigProperty(name = "pools", defaultValue = "1")
//	private String pools;
	
	private String requestId;
	
    @POST
    @Path("random")
    @StoreTime
    public Response random_number() {
    	requestId = prngService.create_id_request();
    	ExecutorService service = Executors.newFixedThreadPool(1);
    	service.execute(new RandomNumberTask());
        return ok(prngService.success_JsonRandomNumber(requestId)).header("Request-Id", requestId).build();
    }
    
    public class RandomNumberTask implements Runnable{
    	
    	@Override
    	public void run() {
				prngService.createNumber(requestId);
    	}
    }
    
    @GET
    @Path("history")
    public Response history() {
    	prngService.getHistory();
    	return ok(prngService.getHistory()).build();
    }
    
    @PUT
    @Path("{requestId}/cancel")
    public Response cancel(@PathParam("requestId") String requestId) {
    	return ok(prngService.cancel(requestId)).build();
    }   
    
    @GET
    @Path("stats")
    public Response stats() {
    	return ok(prngService.stats()).build();
    }
    
    @GET
    @Path("pending")
    public Response pending() {
    	return ok("pending").build();
    }
    
    @PUT
    @Path("threads")
    public Response threads() {
    	return ok("threads").build();
    }
    
    @GET
    public Response index() {
    	return ok("API created by Elton Tomas Laice").build();
    }
}
