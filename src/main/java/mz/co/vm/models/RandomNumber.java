package mz.co.vm.models;

import java.io.Serializable;
import java.util.Random;

/**
 * RandomNumber class
 * 
 * @author Elton Tomas Laice
 *
 */
public class RandomNumber implements Serializable{

	private static final long serialVersionUID = 1L;

	private String requestID;
	
	private long number;
	
	private double process_time;
	
	private String status;
	
	public RandomNumber() {}
	
	public RandomNumber(String requestID) {
		this.requestID = requestID;
		this.status = "CREATED";
		gen_numeber();
	}
	public String getRequestID() {
		return requestID;
	}

	public long getNumber() {
		return number;
	}

	public double getProcess_time() {
		return process_time;
	}
	public void setProcess_time(double process_time) {
		this.process_time = process_time;
	}
	public void setProcess_time(int process_time) {
		this.process_time = process_time;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private void gen_numeber() {
		long number;
		
		do {
			Random rn = new Random();
			number = rn.nextLong();
		} while (number < 3);
		
		this.number = number;
	}
}
