# Project mz.co.vm/back-end-vm

Steps to run this project:

Clone project from `https://gitlab.com/EltonTLaice/System-backend-developer.git`

`cd System-backend-developer`

Then Choose one of the options: 

##Option 1: Started with Open Liberty

run `mvn liberty:run`

open `http://localhost:9080/history`

##Option 2: Docker
1. Start your Docker daemon
2. Execute `./buildAndRun.sh` (Linux/MacOs) or `buildAndRun.bat` (Windows)
3. Wait until Open Liberty is up- and running (e.g. use `docker logs -f CONTAINER_ID`)
