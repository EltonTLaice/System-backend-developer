#!/bin/sh
mvn clean package && docker build -t mz.co.vm/back-end-vm .
docker rm -f back-end-vm || true && docker run -d -p 9080:9080 -p 9443:9443 --name back-end-vm mz.co.vm/back-end-vm
